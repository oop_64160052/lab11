package com.nalinthip.week11;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        System.out.println();

        Plane boeing = new Plane("Boeing", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();
        boeing.drive();
        System.out.println();

        Superman clark = new Superman("Clark");
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.Walk();
        clark.run();
        clark.swim();
        clark.eat();
        clark.sleep();
        ;
        System.out.println();

        Human man1 = new Human("Man");
        man1.eat();
        man1.sleep();
        man1.Walk();
        man1.run();
        man1.swim();
        System.out.println();

        Bat bat1 = new Bat("Ju");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();
        System.out.println();

        Rat rat1 = new Rat("Jerry");
        rat1.eat();
        rat1.sleep();
        rat1.Walk();
        rat1.run();
        rat1.swim();
        System.out.println();

        Dog dog1 = new Dog("Shiro");
        dog1.eat();
        dog1.sleep();
        dog1.Walk();
        dog1.run();
        dog1.swim();
        System.out.println();

        Cat cat1 = new Cat("Tom");
        cat1.eat();
        cat1.sleep();
        cat1.Walk();
        cat1.run();
        cat1.swim();
        System.out.println();

        Snake snake1 = new Snake("Nagini");
        snake1.eat();
        snake1.sleep();
        snake1.swim();
        snake1.crawl();
        System.out.println();

        Crocodile crocodile1 = new Crocodile("George");
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.swim();
        crocodile1.crawl();
        System.out.println();

        Fish fish1 = new Fish("Dory");
        fish1.eat();
        fish1.sleep();
        fish1.swim();
        System.out.println();

        Submarine submarine1 = new Submarine("Suphannahong", "mechanical");
        submarine1.drive();
        submarine1.swim();
        System.out.println();

        Bus bus1 = new Bus("Nakhonchaiair", "machine");
        bus1.drive();
        System.out.println();

        Flyable[] flyables = { bird1, boeing, clark, bat1, };
        System.out.println("------ Flyable ------");
        for (int i = 0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }
        System.out.println();

        Walkable[] walkables = { bird1, clark, man1, rat1, dog1, cat1 };
        System.out.println("------- Walkable -------");
        for (int i = 0; i < walkables.length; i++) {
            walkables[i].Walk();
            walkables[i].run();
        }
        System.out.println();

        Swimable[] swimables = { clark, man1, rat1, dog1, cat1, snake1, crocodile1, fish1, submarine1 };
        System.out.println("------ Swimable ------");
        for (int i = 0; i < swimables.length; i++) {
            swimables[i].swim();
        }
        System.out.println();

        Crawlable[] crawlables = { snake1, crocodile1 };
        System.out.println("------ Crawlable ------");
        for (int i = 0; i < crawlables.length; i++) {
            crawlables[i].crawl();
        }
        System.out.println();

        Driveable[] driveables = { boeing, submarine1, bus1 };
        System.out.println("------ Driveable ------");
        for (int i = 0; i < driveables.length; i++) {
            driveables[i].drive();
        }
        System.out.println();
    }

}
