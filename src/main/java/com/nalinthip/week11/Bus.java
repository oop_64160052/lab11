package com.nalinthip.week11;

public class Bus extends Vehicle implements Driveable {

    public Bus(String name, String engine) {
        super(name, engine);
    }
    @Override
    public String toString() {
        return "Bus (" + this.getName() + ")";
    }

    @Override
    public void drive() {
        System.out.println(this + " drive.");    
    }
    
}
