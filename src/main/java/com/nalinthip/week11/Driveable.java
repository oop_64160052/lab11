package com.nalinthip.week11;

public interface Driveable {
    public void drive();
}
