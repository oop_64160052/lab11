package com.nalinthip.week11;

public class Plane extends Vehicle implements Flyable, Driveable {

    public Plane(String name, String enging) {
        super(name, enging);

    }

    @Override
    public void takeoff() {
        System.out.println(this + " takeoff.");

    }

    @Override
    public void fly() {
        System.out.println(this + " fly.");

    }

    @Override
    public void landing() {
        System.out.println(this + " landing.");
    }

    @Override
    public String toString() {
        return "Plane (" + getName() + ")";
    }

    @Override
    public void drive() {
        System.out.println(this + " drive.");
        
    }
}
