package com.nalinthip.week11;

public class Submarine extends Vehicle implements Swimable, Driveable  {

    public Submarine(String name, String engine) {
        super(name, engine);
    
    }
    @Override
    public String toString() {
        return "Submarine (" + this.getName() + ")";
    }

    @Override
    public void drive() {
        System.out.println(this + " drive.");
        
    }

    @Override
    public void swim() {
        System.out.println(this + " swim.");
        
    }
    
}
